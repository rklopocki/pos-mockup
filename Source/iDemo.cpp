#include <stdio.h>
#include <string.h>
#include <eoslog.h>
#include <svc.h>
#include <svctxo.h>
#include <eeslapi.h>
#include <varrec.h>
#include <devman.h>
#include <version.h>
#include <png\PngDefines.h>
//
#include "iDemo.h"
#include "iDemoEvents.h"
#include "EventDef.h"
#include "Cefunc.h"
#include "CommFunc.h"
#include "DisplayFunc.h"
#include "StringFunc.h"
#include "ZoneFunc.h"
#include "BmpFunc.h"
#include "ServiceFunc.h"
#include "LogLevel.h"

int hConsole = -1;

extern Zone zones[ZONES_COUNT_MAX];

unsigned short uiLength = 0;

char cSenderName[32];
unsigned char ucRecordBuffer[FLEXI_RECORD_SIZE];
char apLogicalName[EESL_APP_LOGICAL_NAME_SIZE];
unsigned long m_Devices=0;

int main(int argc, char*argv[]) {
	EESL_CONTROL_STR gEeslData;
	long lError = -1;
	unsigned long usCustomEvent = 0;
	short shortEvent = 0;
	short shortRetval = 0;
	int j = 0;
	int iRet = 0;
	char keys_buffer[KEYBOARD_BUFFER_LENGTH+1];
	int keys_pending = 0;
	int bytes = 0;
	unsigned char cKey;
	unsigned long NewEvents = 0;
	unsigned long HandledEvents = 0;
	int result = 0;
	int displayWidth = 0;
	int displayHeight = 0;
	char info_buffer[INFO_BUFFER_LENGTH+1];
	char ScreenSize[GET_SCREEN_SIZE_BUF_LEN + 1];
	char TidString[TID_STRING_LENGTH + 1];
	char TsnString[TSN_STRING_LENGTH + 1];
	short RunLoopTerminate = FALSE;
	bool foregroundRun = false;
	char clock_buffer[CLOCK_BUFFER_LENGTH+1];
	bool timerStarted = false;
	int TimerPeriodicID = 0;
	unsigned long TimerTicks = 0;
	int loop = 0;
	short numberOfTasks = 0;
	short receivedBufferSize = 0;
	char ResourceDir[PATH_NAME_LENGTH + 1];
	Rectangle FullScreenRect = { {0, 0}, {240, 320}};
	Rectangle StatusRect = { {0, 0}, {240, 12}};
	Rectangle SourceRect = { {230, 1}, {10, 10}};
	Rectangle SaveRect = { {220, 1}, {10, 10}};
	Rectangle TypeRect = { {210, 1}, {10, 10}};
	Rectangle ProgressInfoRect = { {100, 140}, {40, 40}};
	Rectangle ZoneRect = { {0, 0}, {0, 0}};
	Point Touch = {0, 0};
	bool touchHandled = false;
	EESL_IMM_DATA TaskList[10];
	char ServiceURL[URL_STRING_LENGTH + 1];
	short *frameBufferStatusPtr = NULL;
	short *frameBufferDisplayPtr = NULL;
	int frameBufferSize = 0;
	unsigned long pngHeight;
	unsigned long pngWidth;
	int imageType;
	int resultInt;
	bool ReloadWaiting;
	bool ReloadAllowed;
	HReqDetails ImageResource;
	HReqDetails ZoneResource;
	char Request[HTTP_REQUEST_BUF_LEN];
	HeadersDetails Headers;
	char *Response = NULL;
	int ResponseSize = 0;
	int ResponseLength = 0;
	int Loop;
	int Color;
	int ZoneCount;
	long ApplicationTicks;
	int ImageDataLength = 0;
	char FileNameBuffer[PATH_NAME_LENGTH + 1];
	FILE *fImage = NULL;
	BitmapFileHeader ImageHeader;
	DIBHeader ImageDIB;
	bool LocalFiles;
	bool SaveFiles;
	bool BitmapFiles;
	bool LoadFailed;
	int touchState;
	Point touchStartPoint;
	Point touchEndPoint;
	Point touchMove;
	int showStatus;
	int settingChanged;

	uiLength = 0;
	memset (ucRecordBuffer, 0, sizeof (ucRecordBuffer));
	memset (cSenderName, 0, sizeof (cSenderName));
	strcpy (apLogicalName, IDEMO_APPLICATION_LOGICAL_NAME);

	// Log init
	LOG_INIT(IDEMO_APPLICATION_LOGICAL_NAME, LOGSYS_OS, APPL_LOG_FILTER_ERROR | APPL_LOG_FILTER_WARNING | APPL_LOG_FILTER_INFO | APPL_LOG_FILTER_DATA);
	LOG_PRINTFF(APPL_LOG_FILTER_INFO, "App rebuilt: %s at %s", __DATE__, __TIME__);
	// EESL init
	lError = EESL_Initialise (argv, argc, apLogicalName, &gEeslData);
	if(lError < 0) {
		LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "EESL_Initialise:%d", lError);
		return (int) lError;
	}
	LOG_PRINTFF (APPL_LOG_FILTER_INFO, "EESL_Initialise OK");
	SVC_INFO_PTID (TidString);
	SVC_CS2AZ(TidString, TidString);
	LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "TID: %s", TidString);
	SVC_INFO_SERLNO (TsnString);
	TsnString[11]=ASC_NULL;
	LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "TSN: %s", TsnString);

	TimerTicks = TIMER_TICKS_PERIODIC;
	ReloadWaiting = false;
	ReloadAllowed = false;
	ApplicationTicks = 0;
	CleanResourceHttp(&ImageResource);
	CleanResourceHttp(&ZoneResource);
	SetStringFromConfigWithDefault(ResourceDir, sizeof(ResourceDir), ENV_NAME_RESOURCE_DIR, RESOURCE_DIR_DEFAULT);
	LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Resource dir: %s", ResourceDir);
	SetStringFromConfigWithDefault(ServiceURL, sizeof(ServiceURL), ENV_NAME_SERVICE_URL, SERVICE_URL_DEFAULT);
	LocalFiles = BoolFromConfigWithDefault(ENV_NAME_LOCAL_FILES, LOCAL_FILES_DEFAULT);
	SaveFiles = BoolFromConfigWithDefault(ENV_NAME_SAVE_FILES, SAVE_FILES_DEFAULT);
	BitmapFiles = BoolFromConfigWithDefault(ENV_NAME_BITMAP_FILES, BITMAP_FILES_DEFAULT);
	LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Service URL: %s", ServiceURL);
	//	PaymentVersionString[0] = ASC_NULL;
	while(!RunLoopTerminate) {
		NewEvents = wait_event();
		HandledEvents = 0;
		if((NewEvents != EVT_CONSOLE) && (NewEvents != EVT_TIMER)) {
			LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Returned from wait_event() with value %0lx", NewEvents);
		}
		if (NewEvents!=0) {
			if(NewEvents&EVT_DEACTIVATE) {
				HandledEvents = HandledEvents | EVT_DEACTIVATE;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_DEACTIVATE");
				if(hConsole >= 0) {
					close(hConsole);
					hConsole = -1;
				}
				shortEvent=ACT_EVENT;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Sending RES_COMPLETE_EVENT to %s", DEVICE_MANAGER_LOGICAL_NAME);
				EESL_send_event(DEVICE_MANAGER_LOGICAL_NAME, RES_COMPLETE_EVENT, (unsigned char *) &shortEvent, sizeof (short));
				shortRetval=EESL_send_res_release_event (m_Devices, NULL);
				if(shortRetval!=0) {
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, EESL_SEND_EVENT_FAIL_DEACT);
				}
				foregroundRun=false;
			}
			if((NewEvents&EVT_PIPE)) {
				HandledEvents = HandledEvents | EVT_PIPE;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_PIPE");
				usCustomEvent = EESL_read_cust_evt((unsigned char *)ucRecordBuffer, sizeof(ucRecordBuffer), &uiLength, cSenderName);
				// On the Activate Event Request for the Device which are not present
				if(usCustomEvent == ACT_EVENT) {
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received custom event ACT_EVENT from %s", cSenderName);
					m_Devices = 0;
					shortEvent = 0;
					hConsole = open(DEV_CONSOLE, 0);
					if (hConsole < 0) {
						LOG_PRINTF("%s - %s\n", "Console", "open failed");
					}
					set_event_bit (hConsole, EVT_TOUCH);
					screen_size(ScreenSize);
					displayHeight = (int)ScreenSize[0];
					displayWidth = (int)ScreenSize[1];
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Display height: %d , display width: %d", displayHeight, displayWidth);
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Current display grid: %d", getgrid());
					shortEvent=0;
					foregroundRun=true;
					set_display_coordinate_mode(PIXEL_MODE);
					screen_size(ScreenSize);
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Screen height: %d , width: %d", (int)ScreenSize[0], (int)ScreenSize[1]);
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "PNG Version: %s", getPNGVersion());
					frameBufferStatusPtr = (short *)malloc(StatusRect.size.width * StatusRect.size.height * 2);
					Color = 0;
					for(Loop = 0, Color = 0; Loop < (StatusRect.size.width * StatusRect.size.height); Loop++) {
						frameBufferStatusPtr[Loop]= PIXEL565((Color & 0xF800)>> 11, (Color & 0x07E0) >> 5, (Color & 0x001F));
					}
					frameBufferSize = FullScreenRect.size.height * FullScreenRect.size.width * 2;
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Frame buffer size: %d", frameBufferSize);
					frameBufferDisplayPtr = (short *)malloc(frameBufferSize);
					Color = 0;
					for(Loop = 0, Color = 0; Loop < (FullScreenRect.size.height * FullScreenRect.size.width); Loop++) {
						if(Color < 65566) {
							frameBufferDisplayPtr[Loop]= PIXEL565((Color & 0xF800)>> 11, (Color & 0x07E0) >> 5, (Color & 0x001F));
						} else {
							frameBufferDisplayPtr[Loop]= PIXEL565(31, 63, 31);
						}
						if((Loop % FullScreenRect.size.width) < (16 * 14)) {
							Color++;
						}
					}
					display_frame_buffer(FullScreenRect.origin.x, FullScreenRect.origin.y, FullScreenRect.size.width, FullScreenRect.size.height, frameBufferDisplayPtr);
					ResponseSize = FullScreenRect.size.height * FullScreenRect.size.width * 2 + 1024;
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Response buffer size: %d", ResponseSize);
					Response = (char*)malloc(ResponseSize);
					SplitServiceURL(&ImageResource, ServiceURL);
					ReloadWaiting = true;
					TimerPeriodicID = set_timer(TimerTicks, EVT_TIMER);
				} else if(usCustomEvent == APP_PAYMENT_OK) {
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received custom event APP_PAYMENT_OK from: %s", cSenderName);
				} else {
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "EESL_read_cust_evt() returned %d", usCustomEvent);
				}
			}
			if (NewEvents & EVT_ACTIVATE) {
				HandledEvents = HandledEvents | EVT_ACTIVATE;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_ACTIVATE");
				RunLoopTerminate = TRUE;
				hConsole = open(DEV_CONSOLE, 0);
				set_event_bit (hConsole, EVT_TOUCH);
			}
			if (NewEvents&EVT_CONSOLE) {
				HandledEvents = HandledEvents | EVT_CONSOLE;
//				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_CONSOLE");
			}
			if (NewEvents&EVT_CLK) {
				HandledEvents = HandledEvents | EVT_CLK;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_CLK");
			}
			if (NewEvents & EVT_TIMER) {
				HandledEvents = HandledEvents | EVT_TIMER;
//				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_TIMER");
				ApplicationTicks++;
				if(ApplicationTicks > WAIT_FOR_NETWORK_DELAY_SECONDS) {
					ReloadAllowed = true;
				}
				if(foregroundRun) {
/*
					read_clock(clock_buffer);
					sprintf(info_buffer, "%.2s:%02.2s:%02.2s", clock_buffer+8, clock_buffer+10, clock_buffer+12);
					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Timer at %s", info_buffer);
*/
					if(showStatus > 0) {
						showStatus-- ;
					} else if(showStatus == 0){
						showStatus = -1;
						display_frame_buffer(FullScreenRect.origin.x, FullScreenRect.origin.y, FullScreenRect.size.width, FullScreenRect.size.height, frameBufferDisplayPtr);
					}
				}
				TimerPeriodicID = set_timer(TimerTicks, EVT_TIMER);
			}
			if (NewEvents&EVT_MAG) {
				HandledEvents = HandledEvents | EVT_MAG;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_MAG");
			}
			if (NewEvents&EVT_TOUCH) {
				HandledEvents = HandledEvents | EVT_TOUCH;
//				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_TOUCH");
				touchHandled = false;
				touchState = get_touchscreen(&Touch.x, &Touch.y);
				touchStartPoint.x = Touch.x;
				touchStartPoint.y = Touch.y;
				if(touchState) {
					while(touchState) {
						touchState = get_touchscreen(&Touch.x, &Touch.y);
						if(touchState) {
							touchEndPoint.x = Touch.x;
							touchEndPoint.y = Touch.y;
						}
					}
					touchMove.x = touchStartPoint.x - touchEndPoint.x;
					touchMove.y = touchStartPoint.y - touchEndPoint.y;
					if((touchMove.x > -20) && (touchMove.x < 20) && (touchMove.y > MINIMAL_STATUS_MOVE)) {
//						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Move up detected x: %d y: %d", touchMove.x,  touchMove.y);
						showStatus = -1;
						display_frame_buffer(FullScreenRect.origin.x, FullScreenRect.origin.y, FullScreenRect.size.width, FullScreenRect.size.height, frameBufferDisplayPtr);
						touchHandled = true;
					} else if((touchMove.x > -20) && (touchMove.x < 20) && (touchMove.y < -MINIMAL_STATUS_MOVE)) {
//						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Move down detected x: %d y: %d", touchMove.x,  touchMove.y);
						showStatus = STATUS_DISPLAY_TIME;
						display_frame_buffer(StatusRect.origin.x, StatusRect.origin.y, StatusRect.size.width, StatusRect.size.height, frameBufferStatusPtr);
						if(LocalFiles) {
							iRet = DisplayBitmap(&SourceRect, ResourceDir, BITMAP_NAME_FILE_MODE_LOCAL);
						} else {
							iRet = DisplayBitmap(&SourceRect, ResourceDir, BITMAP_NAME_FILE_MODE_REMOTE);
						}
						if(SaveFiles) {
							iRet = DisplayBitmap(&SaveRect, ResourceDir, BITMAP_NAME_FILE_MODE_SAVE);
						}
						if(BitmapFiles) {
							iRet = DisplayBitmap(&TypeRect, ResourceDir, BITMAP_NAME_IMAGE_TYPE_BMP);
						} else {
							iRet = DisplayBitmap(&TypeRect, ResourceDir, BITMAP_NAME_IMAGE_TYPE_PNG);
						}
						touchHandled = true;
					} else {
//						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Display presseed at x: %d y: %d", touchStartPoint.x, touchStartPoint.y);
						for(Loop = 0; Loop < ZoneCount; Loop++) {
							if(IsRectangleTouched(&(zones[Loop].area), &touchStartPoint)) {
								SplitServiceURL(&ImageResource, zones[Loop].serviceURL);
								ZoneCount = 0;
								ReloadWaiting = true;
								touchHandled = true;
								break;
							}
						}
					}
				} else {
//					LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Pen up detected");					
				}
				if(touchHandled) {
					normal_tone();
				} else {
					error_tone();
				}
			}
			if (NewEvents&EVT_KBD) {
				HandledEvents = HandledEvents | EVT_KBD;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_KBD");
				keys_pending = kbd_pending_count();
				if (keys_pending > 0) { // key_beeps(bool), 
					settingChanged = false;
					bytes = read(hConsole, keys_buffer, KEYBOARD_BUFFER_LENGTH);
					for(j = 0; j < bytes; j++) {
						cKey = keys_buffer[j];
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Character read: 0x%02x\n", cKey);
/*
						if((cKey >= 0xB0)&&(cKey <= 0xB9)) { //	Digit
							error_tone();
						}
*/
						if(cKey == 0x88) { //	Backspace
							ApplicationTicks = WAIT_FOR_NETWORK_DELAY_SECONDS;
							SplitServiceURL(&ImageResource, ServiceURL);
							ReloadWaiting = true;
							normal_tone();
						} else if(cKey == 0x8D) { //	Enter
							for (Loop = 0; Loop < ZoneCount; Loop++) {
								CopyRectangle(&ZoneRect, &(zones[Loop].area));
								DrawFrame(&ZoneRect, 1, COLOR_RED);
							}
							normal_tone();
						} else if(cKey == 0xC0) { //	Cancel + '0'
							sprintf(info_buffer, "Restart key...");
							LOG_PRINTFF(APPL_LOG_FILTER_INFO, info_buffer);
							result = SVC_RESTART("");
							sprintf(info_buffer, "SVC_RESTART () -> %d", result);
							LOG_PRINTFF(APPL_LOG_FILTER_INFO, info_buffer);
							normal_tone();
						} else if(cKey == 0xB7) {	//
							if(BitmapFiles) {
								BitmapFiles = false;
								SetEnv(ENV_NAME_BITMAP_FILES, "0");
							} else {
								BitmapFiles = true;
								SaveFiles = false;
								SetEnv(ENV_NAME_BITMAP_FILES, "1");
							}
							settingChanged = true;
							normal_tone();
						} else if(cKey == 0xB8) {	//
							if(!LocalFiles) {
								if(SaveFiles) {
									SaveFiles = false;
									SetEnv(ENV_NAME_SAVE_FILES, "0");
								} else {
									SaveFiles = true;
									SetEnv(ENV_NAME_SAVE_FILES, "1");
								}
								settingChanged = true;
								normal_tone();
							} else {
								error_tone();
							}
						} else if(cKey == 0xB9) {	//
							if(LocalFiles) {
								LocalFiles = false;
								SetEnv(ENV_NAME_LOCAL_FILES, "0");
							} else {
								LocalFiles = true;
								SaveFiles = false;
								SetEnv(ENV_NAME_LOCAL_FILES, "1");
								SetEnv(ENV_NAME_SAVE_FILES, "0");
							}
							settingChanged = true;
							normal_tone();
						} else {
							error_tone();
						}
					}
				}
				if(settingChanged) {
					display_frame_buffer(StatusRect.origin.x, StatusRect.origin.y, StatusRect.size.width, StatusRect.size.height, frameBufferStatusPtr);
					if(LocalFiles) {
						iRet = DisplayBitmap(&SourceRect, ResourceDir, BITMAP_NAME_FILE_MODE_LOCAL);
					} else {
						iRet = DisplayBitmap(&SourceRect, ResourceDir, BITMAP_NAME_FILE_MODE_REMOTE);
					}
					if(SaveFiles) {
						iRet = DisplayBitmap(&SaveRect, ResourceDir, BITMAP_NAME_FILE_MODE_SAVE);
					}
					if(BitmapFiles) {
						iRet = DisplayBitmap(&TypeRect, ResourceDir, BITMAP_NAME_IMAGE_TYPE_BMP);
					} else {
						iRet = DisplayBitmap(&TypeRect, ResourceDir, BITMAP_NAME_IMAGE_TYPE_PNG);
					}
					showStatus = STATUS_DISPLAY_TIME;
				}
			}
			if (NewEvents&EVT_SHUTDOWN) {
				HandledEvents = HandledEvents | EVT_SHUTDOWN;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_SHUTDOWN");
				RunLoopTerminate = TRUE;
				sprintf(info_buffer, "Shutdown event...");
			}
			if (NewEvents & EVT_SOKT) {
				HandledEvents = HandledEvents | EVT_SOKT;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_SOKT");
			}
			if (NewEvents & EVT_NETWORK) {
				HandledEvents = HandledEvents | EVT_NETWORK;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_NETWORK");
			}
			if (NewEvents & EVT_ICC1_INS) {
				HandledEvents = HandledEvents | EVT_ICC1_INS;
			}
			if (NewEvents&EVT_ICC1_REM) {
				HandledEvents = HandledEvents | EVT_ICC1_REM;
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received EVT_icc1_rem");
			}
			if(NewEvents != HandledEvents) {
				LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Other events not recognized: %lx", NewEvents - HandledEvents);
			}
			if(ReloadWaiting) {
				if(ReloadAllowed) {
					ReloadWaiting = false;
					LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Starting reload image");
					ZoneCount = 0;
					ZoneFromImageResource(&ZoneResource, &ImageResource, ".txt");
					iRet = DisplayBitmap(&ProgressInfoRect, ResourceDir, BITMAP_NAME_INFO_IMAGE_LOADING);
					strcpy(FileNameBuffer, ImageResource.FileName);
					LoadFailed = false;
					if(!LocalFiles) {
						LoadFailed = true;
						if(!SaveFiles) {
							sprintf(FileNameBuffer, "%s.%s", IMAGE_FILE_NAME, SkipField(ImageResource.FileName, ASC_DECIMAL_POINT));
							LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Temporary file name for image: %s", FileNameBuffer);
							DeleteFile(ResourceDir, FileNameBuffer);
						} else {
							LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Local file name for image: %s", FileNameBuffer);
						}
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Host servername: %s", ImageResource.ServerName);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Host server address: %s", ImageResource.ServerAddress);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Host server port: %s", ImageResource.ServerPort);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Protocol: %s", ImageResource.Protocol);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Resource name: %s", ImageResource.ItemName);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "File name: %s", ImageResource.FileName);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Content type: %s", ImageResource.ContentType);
						result = PrepareFileRequest(&ImageResource, Request, sizeof(Request));
						LogFunctionResult("PrepareFileRequest for IMAGE", PREPARE_SUCCESS, &result, NULL);
						if (result == PREPARE_SUCCESS) {
							LOG_PRINTFF(APPL_LOG_FILTER_INFO, Request);
							ResponseLength = ResponseSize;
							if(TimerPeriodicID) {
								clr_timer(TimerPeriodicID);
								TimerPeriodicID = 0;
							}
							result = SendRequestAndReceiveAnswer(&ImageResource, Request, Response, &ResponseLength);
							LogFunctionResult("SendRequestAndReceiveAnswer for IMAGE ", REQUEST_SUCCESS, &result, NULL);
							if (result == REQUEST_SUCCESS) {
								ReadHeaders(&Headers, Response, ResponseLength);
								LOG_PRINTFF(APPL_LOG_FILTER_INFO, "ContentLength: %d", Headers.ContentLength);
								LOG_PRINTFF(APPL_LOG_FILTER_INFO, "ContentType: %s", Headers.ContentType);
								LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Connect: %s", Headers.Connection);
								if(stricmp(Headers.ContentType, HEADER_VALUE_IMAGE_PNG) == 0) {
									LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received PNG type image");
								} else if(stricmp(Headers.ContentType, HEADER_VALUE_IMAGE_BMP) == 0) {
									LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Received BMP type image");
								}
								if(CheckFileType(FileNameBuffer, &Headers)) {
									SaveDataFile(ResourceDir, FileNameBuffer, Headers.ContentLength, Response, ResponseLength);
									LoadFailed = false;
								}
							}
						}
					}
					if(!LoadFailed) {
						if(ImageResource.ContentType == IMAGE_PNG_FILE_EXT) {
							pngHeight = 0;
							pngWidth = 0;
							imageType = 0;
							resultInt = 0;
							LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Image name: %s", FileNameBuffer);
							resultInt = get_png_data(FileNameBuffer, &pngHeight, &pngWidth, &imageType);
							if(resultInt == 0) {
								LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "PNG data (%d) width: %ld , height: %ld , type: %d", resultInt, pngWidth, pngHeight, imageType);
								if((pngWidth == 240) && (pngHeight == 320)) {
									resultInt = get_png_display_fb(FileNameBuffer, frameBufferDisplayPtr, FullScreenRect.size.height, FullScreenRect.size.width);
									if(resultInt != 0) {
										LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Failed geting png display frame buffer");
										LoadFailed = true;
									} else {
										LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Success geting png display frame buffer");
									}
								} else {
									LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Image size does not fit frame buffer size");
									LoadFailed = true;
								}
							} else {
								LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Image data can not be read - error: ");
								LoadFailed = true;
							}
						} else if(ImageResource.ContentType == IMAGE_BMP_FILE_EXT) {
							fImage = LoadBmpInfo(&ImageHeader, &ImageDIB, ResourceDir, FileNameBuffer);
							if(fImage != NULL) {
								ImageDataLength = LoadImageData((char*)frameBufferDisplayPtr, frameBufferSize, &ImageHeader, &ImageDIB, fImage);
								LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Image data size: %d", ImageDataLength);
								if(ImageDataLength != frameBufferSize) {
									LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Image data does not fit frame buffer size");
									LoadFailed = true;
								}
							} else {
								LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Image data file does mot exist or is wrong");
								LoadFailed = true;
							}
						} else {
							LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Image type is not supported: %s", ImageResource.ContentType);
							LoadFailed = true;
						}
					}
					if(!LoadFailed) {
						iRet = DisplayBitmap(&ProgressInfoRect, ResourceDir, BITMAP_NAME_INFO_IMAGE_LOAD_OK);
						display_frame_buffer(FullScreenRect.origin.x, FullScreenRect.origin.y, FullScreenRect.size.width, FullScreenRect.size.height, frameBufferDisplayPtr);
					} else { 
						iRet = DisplayBitmap(&ProgressInfoRect, ResourceDir, BITMAP_NAME_INFO_IMAGE_LOAD_FAIL);
					}
					sprintf(FileNameBuffer, "%s.%s", ImageResource.FileName, ZONE_FILE_EXT);
					LoadFailed = false;
					iRet = DisplayBitmap(&ProgressInfoRect, ResourceDir, BITMAP_NAME_INFO_ZONES_LOADING);
					if(!LocalFiles) {
						LoadFailed = true;
						if(!SaveFiles) {
							sprintf(FileNameBuffer, "%s.%s", IMAGE_FILE_NAME, ZONE_FILE_EXT);
							LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Temporary file name for zones: %s", FileNameBuffer);
							DeleteFile(ResourceDir, FileNameBuffer);
						} else {
							LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Local file name for zones: %s", FileNameBuffer);
						}
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Host servername: %s", ZoneResource.ServerName);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Host server address: %s", ZoneResource.ServerAddress);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Host server port: %s", ZoneResource.ServerPort);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Protocol: %s", ZoneResource.Protocol);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Resource name: %s", ZoneResource.ItemName);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "File name: %s", ZoneResource.FileName);
						LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Content type: %s", ZoneResource.ContentType);
						result = PrepareFileRequest(&ZoneResource, Request, sizeof(Request));
						LogFunctionResult("PrepareFileRequest for ZONES", PREPARE_SUCCESS, &result, NULL);
						if (result == PREPARE_SUCCESS) {
							LOG_PRINTFF(APPL_LOG_FILTER_INFO, Request);
							ResponseLength = ResponseSize;
							result = SendRequestAndReceiveAnswer(&ZoneResource, Request, Response, &ResponseLength);
							LogFunctionResult("SendRequestAndReceiveAnswer for ZONES ", REQUEST_SUCCESS, &result, NULL);
							if (result == REQUEST_SUCCESS) {
								ReadHeaders(&Headers, Response, ResponseLength);
								LOG_PRINTFF(APPL_LOG_FILTER_INFO, "ContentLength: %d", Headers.ContentLength);
								LOG_PRINTFF(APPL_LOG_FILTER_INFO, "ContentType: %s", Headers.ContentType);
								LOG_PRINTFF(APPL_LOG_FILTER_INFO, "Connect: %s", Headers.Connection);
								if(CheckFileType(FileNameBuffer, &Headers)) {
									SaveDataFile(ResourceDir, FileNameBuffer, Headers.ContentLength, Response, ResponseLength);
									LoadFailed = false;
								}
							}
						}
					}
					if(!LoadFailed) {
						ZoneCount = LoadZones(ResourceDir, FileNameBuffer);
						if(ZoneCount < 0) {
							LoadFailed = true;
							LOG_PRINTFF(APPL_LOG_FILTER_ERROR, "Zones file does mot exist or is wrong");
						}
					}
					if(!LoadFailed) {
						iRet = DisplayBitmap(&ProgressInfoRect, ResourceDir, BITMAP_NAME_INFO_ZONES_LOAD_OK);
						if(ImageDataLength == frameBufferSize) {
							display_frame_buffer(FullScreenRect.origin.x, FullScreenRect.origin.y, FullScreenRect.size.width, FullScreenRect.size.height, frameBufferDisplayPtr);
						}
					} else {
						iRet = DisplayBitmap(&ProgressInfoRect, ResourceDir, BITMAP_NAME_INFO_ZONES_LOAD_FAIL);
					}
					if(!LocalFiles) {
						TimerPeriodicID = set_timer(TimerTicks, EVT_TIMER);
					}
				}
			}
		}
	}
	return 0;
}
	/*
	 #define EVT_KBD         (1L<<0 )    // keyboard input
	 #define EVT_CONSOLE     (1L<<1 )    // display output complete
	 #define EVT_CLK         (1L<<2 )    // one second clock ticks
	 #define EVT_TIMER       (1L<<3 )    // user-defined timer
	 #define EVT_PIPE        (1L<<4 )    // pipe I/O
	 #define EVT_SOKT        (1L<<5 )    // socket I/O
	 #define EVT_NETWORK     (1L<<5 )    // compatibility
	 #define EVT_COM1        (1L<<6 )    // com 1 I/O
	 #define EVT_COM2        (1L<<7 )    // com 2 I/O
	 #define EVT_COM3        (1L<<8 )    // com 3 I/O
	 #define EVT_COM4        (1L<<9 )    // com 4 I/O
	 #define EVT_COM5        (1L<<10)    // com 5 I/O
	 #define EVT_COM8        (1L<<11)    // com 8 I/O
	 #define EVT_MAG         (1L<<12)    // mag card swipe
	 #define EVT_ICC1_INS    (1L<<13)    // customer smart card I/O
	 #define EVT_SDMEM       (1L<<14)    // SD memory card
	 #define EVT_USB_CLIENT   (1L<<15)    // USB-slave events
	 #define EVT_ACTIVATE    (1L<<16)    // now can use console handle
	 #define EVT_DEACTIVATE  (1L<<17)    // now cannot use console handle
	 #define EVT_BAR         (1L<<18)    // Barcode
	 #define EVT_ICC1_REM    (1L<<19)    // customer smart card removed
	 #define EVT_REMOVED     (1L<<20)    // UPT case removal event
	 #define EVT_IFD_READY   (1L<<21)    // smart card response available
	 #define EVT_IFD_TIMEOUT (1L<<22)    // smart card response timed out
	 #define EVT_USB         (1L<<23)    // any (usually, "the") USB device
	 #define EVT_COM6        (1L<<24)    // com 6 I/O
	 #define EVT_COM7        (1L<<25)    // com 7 I/O
	 #define EVT_WLN         (1L<<26)    // Wireless LAN WiFi
	 //#define open          (1L<<27)    // open event
	 #define EVT_BIO         (1L<<28)    // MSO300 biometric device
	 #define EVT_USER        (1L<<29)    // post_user_event summary bit
	 #define EVT_SHUTDOWN    (1L<<30)    // powering off soon: all awake!
	 #define EVT_SYSTEM      ((long)(1UL<<31)) // dock, unzip completion, ...
	 */

