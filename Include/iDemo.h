#ifndef __IDEMO__
#define __IDEMO__

#include "DisplayFunc.h"
#include "StringFunc.h"

//	Missing defs - VX Development
#define EVT_TOUCH	                     		0x02000000	//	33554432

#define APPL_VERSION_STRING_LENGTH				20
#define TID_STRING_LENGTH	20
#define TSN_STRING_LENGTH	20

#define IDEMO_APPLICATION_LOGICAL_NAME			"IDEMO"
#define DEVICE_MANAGER_LOGICAL_NAME				"DEVMAN"

#define TERMINAL_IDENTIFIER_STRING_LEN			8
#define TERMINAL_SERIAL_NUMBER_STRING_LEN		11

#define ENV_NAME_RESOURCE_DIR					"#ResDir"
#define ENV_NAME_SERVICE_URL					"#ServiceURL"
#define ENV_NAME_LOCAL_FILES					"#LocalFiles"
#define ENV_NAME_SAVE_FILES						"#SaveFiles"
#define ENV_NAME_BITMAP_FILES					"#BitmapFies"

#define RESOURCE_DIR_DEFAULT					"F:2/"
#define SERVICE_URL_DEFAULT						"http://83.220.107.62/files/start.png"
#define LOCAL_FILES_DEFAULT						"0"
#define SAVE_FILES_DEFAULT						"0"
#define BITMAP_FILES_DEFAULT					"0"

#define CLOCK_BUFFER_LENGTH						15
#define INITIAL_IMAGE_NAME						"start.png"

#define ZONES_FILE_NAME							"zones"
#define IMAGE_FILE_NAME							"image"
#define ZONE_FILE_EXT							"txt"
#define IMAGE_PNG_FILE_EXT						"png"
#define IMAGE_BMP_FILE_EXT						"bmp"

#define STATUS_DISPLAY_TIME						2
#define MINIMAL_STATUS_MOVE						150
#define KEYBOARD_BUFFER_LENGTH					20

#define FLEXI_RECORD_SIZE   					300
#define INFO_BUFFER_LENGTH						128

#define READING_VALUE_MAX_LENGTH				20

#define TIMER_TICKS_PERIODIC					1000
#define WAIT_FOR_NETWORK_DELAY_SECONDS			10

#endif
